import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import Palette, { defaultPalette } from "./contexts/Palette";

const app = (
  <BrowserRouter>
    <Palette.Provider value={defaultPalette}>
      <App />
    </Palette.Provider>
  </BrowserRouter>
);
ReactDOM.render(app, document.getElementById("root"));
