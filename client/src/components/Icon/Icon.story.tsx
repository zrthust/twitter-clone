import React from "react";
import Icon from "./Icon";
import { withKnobs } from "@storybook/addon-knobs";

export default { title: "Components/Icon", decorators: [withKnobs] };

export const Logo = () => (
  <Icon
    type="logo"
    color="primary"
    style={{
      height: "50px"
    }}
  />
);
export const Chat = () => (
  <Icon
    type="chat"
    color="primary"
    style={{
      height: "50px"
    }}
  />
);
export const Search = () => (
  <Icon
    type="search"
    color="primary"
    style={{
      height: "50px"
    }}
  />
);
export const People = () => (
  <Icon
    type="people"
    color="primary"
    style={{
      height: "50px"
    }}
  />
);

export const Camera = () => (
  <Icon
    type="camera"
    color="primary"
    style={{
      height: "50px"
    }}
  />
);
