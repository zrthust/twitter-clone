import React from "react";
import Button from "./index";
import { render, fireEvent, cleanup } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";

describe("[Component] Button testing", () => {
  afterEach(cleanup);
  it("Should click the passed event", () => {
    const mockOnClick = jest.fn();
    const buttonText = "It works!";
    const { getByText } = render(
      <BrowserRouter>
        <Button buttonType="primary" onClick={mockOnClick}>
          {buttonText}
        </Button>
      </BrowserRouter>
    );
    fireEvent.click(getByText(buttonText));
    expect(mockOnClick).toBeCalledTimes(1);
  });
});
