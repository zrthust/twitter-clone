import React, { useContext } from "react";
import styled, { css } from "styled-components";
import Palette, { PaletteProps } from "../../contexts/Palette";
import { Link, useLocation } from "react-router-dom";

type ButtonInterface = PaletteProps & { styleType: string };

const buttonStyle = css<ButtonInterface>`
  min-height: 39px;
  height: 0;
  padding-left: 1em;
  padding-right: 1em;
  border-radius: 9999px;
  outline: 0;
  border: 0;
  cursor: pointer;
  user-select: none;
  position: relative;
  overflow: hidden;
  font-size: 15px;
  font-weight: 700;
  line-height: 20px;
  text-decoration: none;
  display: flex;
  text-align: center;
  align-items: center;
  justify-content: center;
  transition-duration: 0.2s;
  transition-property: background-color;
  ${props => {
    if (props.styleType === "primary") {
      return css`
        background-color: ${props.palette.primaryColor};
        color: white;
        &:disabled {
          background-color: ${props.palette.secondaryColor}!important;
        }
        &:hover {
          background-color: ${props.palette.harderPrimaryColor};
        }
      `;
    } else if (props.styleType === "secondary") {
      return css`
        background-color: white;
        color: ${props.palette.primaryColor};
        border: 1px solid ${props.palette.primaryColor};
        &:disabled {
          opacity: 0.5;
          background-color: white !important;
        }
        &:hover {
          background-color: ${props.palette.shallowPrimaryColor};
        }
      `;
    } else {
      return css`
        background-color: ${props.palette.buttonGray};
        color: black;
        &:disabled {
          opacity: 0.5;
        }
        &:hover {
          background-color: ${props.palette.buttonGrayHover};
        }
      `;
    }
  }};
`;

const StyledButton = styled.button<ButtonInterface>`
  ${buttonStyle};
`;

const LinkContainer = styled.div<ButtonInterface>`
  ${buttonStyle};
  overflow: hidden;
  padding: 0;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;
  width: 999px;
  height: 100%;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 1em;
  padding-right: 1em;
`;

interface ButtonProps {
  buttonType: "primary" | "secondary" | "tertiary";
  link?: string;
  modal?: boolean;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  children: React.ReactNode;
  style?: object;
  disabled?: boolean;
}

const Button = ({
  buttonType,
  children,
  link,
  modal,
  onClick,
  disabled,
  ...rest
}: ButtonProps) => {
  const palette = useContext(Palette);
  let location = useLocation();
  return link ? (
    <LinkContainer palette={palette} styleType={buttonType} {...rest}>
      <StyledLink
        to={{
          pathname: link,
          state: modal ? { background: location } : undefined
        }}
      >
        {children}
      </StyledLink>
    </LinkContainer>
  ) : (
    <StyledButton
      palette={palette}
      styleType={buttonType}
      disabled={disabled}
      onClick={onClick}
      {...rest}
    >
      {children}
    </StyledButton>
  );
};

export default Button;
