import React from "react";
import Button from "./Button";
import { boolean, text, withKnobs } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { BrowserRouter } from "react-router-dom";
import { withA11Y } from '@storybook/addon-a11y';

export default { title: "Components/Button", decorators: [withKnobs, withA11Y] };

export const Primary = () => (
  <BrowserRouter>
    <Button
      buttonType="primary"
      onClick={action("You clicked the button")}
      disabled={boolean("Disabled", false)}
    >
      {text("Text", "Sign up")}
    </Button>
  </BrowserRouter>
);

export const Secondary = () => (
  <BrowserRouter>
    <Button
      buttonType="secondary"
      onClick={action("You clicked the button")}
      disabled={boolean("Disabled", false)}
    >
      {text("Text", "Log in")}
    </Button>
  </BrowserRouter>
);
