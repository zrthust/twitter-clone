import React, { useContext } from "react";
import Palette, { PaletteProps } from "../../contexts/Palette";
import { Link as RouterLink } from "react-router-dom";
import styled, { css } from "styled-components";

const Container = styled.div``;
const sharedStyle = css<PaletteProps>`
  color: ${props => props.palette.primaryColor};
  text-decoration: none;
  font-size: 15px;
  font-weight: 400;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;
const Paragraph = styled.p<PaletteProps>`
  ${sharedStyle};
`;
const StyledLink = styled(RouterLink)<PaletteProps>`
  ${sharedStyle};
`;

interface LinkProps {
  to?: string;
  style?: object;
  children: React.ReactNode;
}
const Link = ({ to, children, ...rest }: LinkProps) => {
  const palette = useContext(Palette);
  return (
    <Container>
      {to ? (
        <StyledLink to={to} palette={palette} {...rest}>
          {children}
        </StyledLink>
      ) : (
        <Paragraph palette={palette} {...rest}>
          {children}
        </Paragraph>
      )}
    </Container>
  );
};

export default Link;
