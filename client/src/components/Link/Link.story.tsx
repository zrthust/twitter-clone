import React from "react";
import Link from "./Link";
import { text, withKnobs } from "@storybook/addon-knobs";
import { withA11Y } from "@storybook/addon-a11y";

export default { title: "Components/Link", decorators: [withKnobs, withA11Y] };

export const Default = () => <Link>{text("Text", "I am a link")}</Link>;
