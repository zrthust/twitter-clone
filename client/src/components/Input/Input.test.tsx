import React from "react";
import Input from "./Input";
import { cleanup, render, fireEvent } from "@testing-library/react";

describe("[Component] Input testing", () => {
  afterEach(cleanup);
  it("Should pass the input", () => {
    const mockHandleClick = jest.fn();
    const inputText = "I'm typing...";
    const labelText = "My Label";
    const { getByLabelText } = render(
      <Input type="text" label={labelText} handleChange={mockHandleClick} />
    );
    const input = getByLabelText(labelText);
    fireEvent.change(input, { target: { value: inputText } });
    // @ts-ignore
    expect(input.value).toBe(inputText);
  });
});
