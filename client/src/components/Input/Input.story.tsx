import React from "react";
import { text, withKnobs } from "@storybook/addon-knobs";
import Input from "./Input";
import { action } from "@storybook/addon-actions";
import { withA11Y } from "@storybook/addon-a11y";

export default { title: "Components/Input", decorators: [withKnobs, withA11Y] };

export const Text = () => (
  <Input
    type="text"
    label={text("Label", "Username or email")}
    handleChange={action("You are writing")}
  />
);
export const Password = () => (
  <Input
    type="password"
    label={text("Label", "Password")}
    handleChange={action("You are writing")}
  />
);

export const Error = () => (
  <Input
    type="text"
    label="Name"
    handleChange={action("You are writing")}
    errorMessage={text("Error Message", "Whatever you do, this is an error.")}
  />
);
