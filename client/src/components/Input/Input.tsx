import React, { useContext } from "react";
import Palette, { PaletteProps } from "../../contexts/Palette";
import styled from "styled-components";

type LabelProps = PaletteProps & { errorMessage: string | undefined };
const StyledLabel = styled.label<LabelProps>`
  font-size: 15px;
  font-weight: 400;
  padding-top: 5px;
  padding-left: 10px;
  padding-right: 10px;
  color: ${props =>
    props.errorMessage ? props.palette.error : props.palette.normalGray};
`;

const ErrorText = styled.p<PaletteProps>`
  color: ${props => props.palette.error};
  margin: 0;
  padding-top: 5px;
  padding-right: 10px;
  padding-left: 10px;
  font-size: 15px;
`;

const StyledInput = styled.input<PaletteProps>`
  background-color: transparent;
  border: 0;
  outline: none;
  padding: 5px 10px 5px;
  font-size: 19px;
  color: #14171a;
`;

const InputContainer = styled.div<LabelProps>`
  display: flex;
  flex-direction: column;
  background-color: ${props => props.palette.lightGray};
  border-bottom: 2px solid
    ${props =>
      props.errorMessage
        ? props.palette.error
        : props.palette.normalGray}!important;
  border-radius: 2px;
  &:focus-within {
    border-color: ${props =>
      props.errorMessage
        ? props.palette.error
        : props.palette.primaryColor}!important;
    ${StyledLabel} {
      color: ${props =>
        props.errorMessage
          ? props.palette.error
          : props.palette.primaryColor}!important;
    }
  }
`;

const Container = styled.div`
  margin: 0;
  padding: 0;
`;

interface InputProps {
  type: string;
  label: string;
  handleChange: (event: React.FormEvent<HTMLInputElement>) => void;
  maxInputLength?: number;
  errorMessage?: string;
}
const Input = ({
  type,
  label,
  handleChange,
  maxInputLength,
  errorMessage,
  ...rest
}: InputProps) => {
  const palette = useContext(Palette);
  return (
    <Container>
      <InputContainer palette={palette} errorMessage={errorMessage}>
        <StyledLabel
          htmlFor={label}
          palette={palette}
          errorMessage={errorMessage}
        >
          {label}
        </StyledLabel>
        <StyledInput
          type={type}
          id={label}
          palette={palette}
          onChange={handleChange}
          maxLength={maxInputLength || 99}
          {...rest}
        />
      </InputContainer>
      {errorMessage ? (
        <ErrorText palette={palette}>{errorMessage}</ErrorText>
      ) : null}
    </Container>
  );
};

export default Input;
