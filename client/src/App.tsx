import React from "react";
import { createGlobalStyle } from "styled-components";
import { Switch, Route, useLocation } from "react-router-dom";
import { LandingPage } from "./pages/Landing";
import { LoginPage } from "./pages/Login";
import { RegisterPage } from "./pages/Register";

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }
  body {
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    font-size: 15px;
    margin: 0 15px 0 0;
  }
  p {
    margin:0;
  }
  h1 {
    margin: 0;
  }
`;

function App() {
  let location = useLocation();
  // @ts-ignore
  let background = location.state && location.state.background;
  return (
    <>
      <GlobalStyle />
      <Switch location={background || location}>
        <Route path="/" exact>
          <LandingPage />
        </Route>
        <Route path="/login">
          <LoginPage />
        </Route>
        <Route path="/register">
          <RegisterPage />
        </Route>
      </Switch>
      {background && (
        <Route path="/register">
          <RegisterPage />
        </Route>
      )}
    </>
  );
}

export default App;
