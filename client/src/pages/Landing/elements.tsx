import styled from "styled-components";
import { PaletteProps } from "../../contexts/Palette";
import Icon from "../../components/Icon";

export const Container = styled.main`
  display: flex;
  flex-basis: 0;
  flex-grow: 1;
  flex-shrink: 1;
  flex-direction: row;
  height: 95vh;
  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export const Section = styled.section<PaletteProps>`
  flex: 50%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  position: relative;
  overflow: hidden;
  padding: 14px;
  &:first-child {
    background-color: ${props => props.palette.secondaryColor};
  }

  @media (max-width: 800px) {
    &:first-child {
      order: 2;
    }
  }
`;

// Interest Area

export const BackgroundIcon = styled(Icon)`
  position: absolute;
  max-width: none;
  top: -30vh;
  right: -50vh;
  height: 160vh;
  z-index: 1;
  @media (max-width: 800px) {
    top: -10vh;
    right: -10vh;
    height: 60vh;
  }
`;

export const Interests = styled.div`
  display: flex;
  flex-direction: column;
  z-index: 2;
  max-width: 380px;
  align-self: center;
  margin: 0;
`;

export const Interest = styled.div`
  margin-bottom: 40px;
  display: flex;
  flex-direction: row;
  align-items: center;
  box-sizing: border-box;
  position: relative;
  &:last-child {
    margin-bottom: 0;
  }
`;

export const InterestIcon = styled(Icon)`
  font-size: 23px;
  color: white;
  fill: white;
  user-select: none;
  position: relative;
  max-width: 100%;
  height: 1.25em;
  display: inline-block;
`;

export const InterestText = styled.div`
  font-size: 19px;
  color: white;
  font-weight: 700;
  margin-left: 15px;
  line-height: 30px;
  overflow-wrap: break-word;
`;

export const InterestTextContent = styled.span`
  line-height: 1.3125;
`;

// Action Area

export const ActionArea = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
`;

export const LoginArea = styled.div`
  position: absolute;
  top: 15px;
  left: calc((50% - 190px) - 15px);
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  justify-content: center;
  @media (max-width: 1155px) {
    display: none;
  }
  @media (max-width: 1400px) {
    left: 20px;
  }
`;

export const RegisterArea = styled.div`
  max-width: 380px;
  align-self: center;
  display: flex;
  flex-direction: column;
`;

export const ActionIcon = styled(Icon)`
  height: 2.75rem;
  align-self: flex-start;
`;

export const ActionTitle = styled.h1`
  font-size: 30px;
  font-weight: 700;
  line-height: 39px;
  margin: 20px 0 0;
`;

export const ActionParagraph = styled.p`
  font-size: 15px;
  line-height: 20px;
  font-weight: 700;
  margin-top: 59px;
  margin-bottom: 15px;
`;

export const InputWrapper = styled.div`
  width: 210px;
  margin-right: 15px;
`;
