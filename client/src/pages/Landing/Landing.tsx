import React, { useContext, useEffect, useState } from "react";
import {
  ActionArea,
  ActionIcon,
  ActionParagraph,
  ActionTitle,
  BackgroundIcon,
  Container,
  InputWrapper,
  Interest,
  InterestIcon,
  Interests,
  InterestText,
  InterestTextContent,
  LoginArea,
  RegisterArea,
  Section
} from "./elements";
import Palette from "../../contexts/Palette";
import Button from "../../components/Button";
import Input from "../../components/Input";
import Link from "../../components/Link";

const Landing = () => {
  const palette = useContext(Palette);
  const [value, setValue] = useState("");
  const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
    setValue(e.currentTarget.value);
  };
  useEffect(() => {
    console.log(value);
  }, [value]);
  return (
    <Container>
      <Section palette={palette}>
        <BackgroundIcon type="logo" color="primary" />
        <Interests>
          <Interest>
            <InterestIcon type="search" color="white" />
            <InterestText>
              <InterestTextContent>Follow your interests.</InterestTextContent>
            </InterestText>
          </Interest>
          <Interest>
            <InterestIcon type="people" color="white" />
            <InterestText>
              <InterestTextContent>
                Hear what people are talking about.
              </InterestTextContent>
            </InterestText>
          </Interest>
          <Interest>
            <InterestIcon type="chat" color="white" />
            <InterestText>
              <InterestTextContent>Join the conversation.</InterestTextContent>
            </InterestText>
          </Interest>
        </Interests>
      </Section>
      <Section palette={palette}>
        <ActionArea>
          <LoginArea>
            <InputWrapper>
              <Input
                label="Phone, email, or username"
                type="text"
                handleChange={handleChange}
              />
            </InputWrapper>
            <InputWrapper>
              <Input
                label="Password"
                type="password"
                handleChange={handleChange}
              />
              <Link
                to="/forgot-pw"
                style={{
                  position: "absolute",
                  marginTop: "5px",
                  marginLeft: "10px",
                  lineHeight: "19.4px",
                  fontSize: "13px"
                }}
              >
                Forgot password?
              </Link>
            </InputWrapper>
            <Button
              buttonType="secondary"
              onClick={e => console.log(e)}
              style={{
                minHeight: "39px",
                height: "0px",
                alignSelf: "center"
              }}
            >
              Log in
            </Button>
          </LoginArea>
          <RegisterArea>
            <ActionIcon type="logo" color="primary" />
            <ActionTitle>
              See what’s happening in the world right now
            </ActionTitle>
            <ActionParagraph>Join Twitter today.</ActionParagraph>
            <Button
              buttonType="primary"
              onClick={e => console.log(e)}
              style={{ marginBottom: "15px" }}
              link="/register"
              modal={true}
            >
              Sign up
            </Button>
            <Button
              buttonType="secondary"
              onClick={e => console.log(e)}
              style={{ marginBottom: "15px" }}
              link="/login"
            >
              Log in
            </Button>
          </RegisterArea>
        </ActionArea>
      </Section>
    </Container>
  );
};

export default Landing;
