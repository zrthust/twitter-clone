import React, { useContext } from "react";
import {
  ButtonWrapper,
  Container,
  Content,
  ContentWrapper,
  Paragraph,
  Title
} from "./elements";
import Button from "../../../components/Button";
import Palette from "../../../contexts/Palette";

const VerificationModal = () => {
  const palette = useContext(Palette);
  return (
    <Container>
      <ContentWrapper>
        <Content>
          <Title>Verify phone</Title>
          <Paragraph palette={palette}>
            We'll text your verification code to 0767300623. Standard SMS fees
            may apply.
          </Paragraph>
          <ButtonWrapper>
            <Button
              buttonType="tertiary"
              style={{
                minWidth: "calc(62.79px)",
                flexGrow: "1",
                marginRight: "15px"
              }}
            >
              Edit
            </Button>
            <Button
              buttonType="primary"
              style={{
                minWidth: "calc(62.79px)",
                flexGrow: "1"
              }}
            >
              OK
            </Button>
          </ButtonWrapper>
        </Content>
      </ContentWrapper>
    </Container>
  );
};

export default VerificationModal;
