import styled from "styled-components";
import { PaletteProps } from "../../../contexts/Palette";

export const Container = styled.div`
  position: fixed;
  width: 100%;
  height: 100vh;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 1;
  display: flex;
  flex-direction: column;
  font-size: 15px;
`;

export const ContentWrapper = styled.div`
  position: fixed;
  top: 0;
  z-index: 999;
  width: 320px;
  min-height: 177px;
  max-width: 80vw;
  align-self: center;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 600px;
  padding: 30px 20px;
  display: flex;
  flex-basis: auto;
  flex-direction: column;
  flex-shrink: 1;
  align-items: center;
  overflow-y: auto;
  border-radius: 14px;
  background-color: white;
`;

export const Title = styled.h1`
  margin-bottom: 10px;
  font-size: 19px;
  font-weight: bold;
`;

export const Paragraph = styled.p<PaletteProps>`
  font-size: 15px;
  font-weight: 400;
  margin-bottom: 20px;
  overflow-wrap: break-word;
  line-height: 1.3125;
  text-align: center;
  color: ${props => props.palette.normalGray};
`;

export const ButtonWrapper = styled.div`
  display: flex;
  width: 100%;
  align-items: stretch;
  flex-basis: auto;
  flex-shrink: 0;
  margin: 0;
`;
