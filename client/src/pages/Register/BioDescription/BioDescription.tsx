import React, { useContext } from "react";
import { InputWrapper, Paragraph, Title } from "../elements";
import Palette from "../../../contexts/Palette";
import Input from "../../../components/Input";
import { CharacterTracker } from "../RegisterData/elements";

const BioDescription = () => {
  const palette = useContext(Palette);
  return (
    <>
      <Title>Describe yourself</Title>
      <Paragraph palette={palette}>
        What makes you spacial? Don't think too hard, just have fun with it.
      </Paragraph>
      <InputWrapper>
        <Input type="text" label="Your bio" handleChange={e => console.log} />
        <CharacterTracker palette={palette}>0/160</CharacterTracker>
      </InputWrapper>
    </>
  );
};

export default BioDescription;
