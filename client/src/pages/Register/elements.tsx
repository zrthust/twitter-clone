import styled from "styled-components";
import { PaletteProps } from "../../contexts/Palette";

export const Container = styled.div`
  position: fixed;
  background-color: rgba(0, 0, 0, 0.4);
  left: 0;
  top: 0;
  width: 100%;
  height: 100vh;
  z-index: 999;
  display: flex;
  align-items: center;
  justify-items: center;
  align-content: center;
  justify-content: center;
  @media (max-width: 1000px) {
    background-color: white;
    align-items: flex-start;
  }
`;

export const Content = styled.section`
  min-width: 600px;
  min-height: 400px;
  height: 650px;
  max-width: 80vw;
  max-height: 90vh;
  overflow: hidden;
  box-sizing: border-box;
  background-color: rgba(255, 255, 255, 1);
  border-radius: 14px;
`;

export const HeaderSection = styled.div`
  height: 53px;
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const MainSection = styled.div`
  margin-right: 30px;
  margin-left: 30px;
`;

export const Title = styled.h1`
  font-size: 23px;
  color: black;
  font-weight: bold;
  margin-bottom: 15px;
  margin-top: 15px;
  user-select: none;
  cursor: text;
`;

export const Paragraph = styled.p<PaletteProps>`
  font-size: 15px;
  font-weight: 400;
  overflow-wrap: break-word;
  line-height: 1.3125;
  color: ${props => props.palette.normalGray};
  user-select: none;
  cursor: text;
`;

export const InputWrapper = styled.div`
  padding-bottom: 5px;
  padding-top: 10px;
  display: flex;
  flex-direction: column;
`;
