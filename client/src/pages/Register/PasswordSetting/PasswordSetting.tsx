import React, { useContext } from "react";
import Palette from "../../../contexts/Palette";
import { InputWrapper, Paragraph, Title } from "../elements";
import Input from "../../../components/Input";
import Link from "../../../components/Link";

const PasswordSetting = () => {
  const palette = useContext(Palette);

  return (
    <div>
      <Title>You'll need a password</Title>
      <Paragraph palette={palette}>
        Make sure it's 6 characters or more.
      </Paragraph>
      <InputWrapper>
        <Input
          type="password"
          label="Password"
          handleChange={e => console.log}
        />
      </InputWrapper>
      <Link
        style={{
          padding: "0 10px"
        }}
      >
        Reveal Password
      </Link>
    </div>
  );
};

export default PasswordSetting;
