import styled from "styled-components";
import { PaletteProps } from "../../../contexts/Palette";
import Icon from "../../../components/Icon";

export const ActionWrapper = styled.div<PaletteProps>`
  min-width: 39px;
  min-height: 39px;
  height: 0;
  transition-duration: 0.2s;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 999px;
  cursor: pointer;
  &:hover {
    background-color: ${props => props.palette.shallowPrimaryColor};
  }
  &:active {
    background-color: ${props => props.palette.harderShallowPrimaryColor};
  }
`;

export const ArrowIcon = styled(Icon)`
  height: 1.5em;
`;

export const ActionText = styled.p<PaletteProps>`
  border-width: 1px;
  padding: 0 15px;
  font-size: 15px;
  font-weight: bold;
  color: ${props => props.palette.primaryColor};
  user-select: none;
`;

export const LogoIcon = styled(Icon)`
  height: 1.6rem;
  padding-left: 40px;
`;
