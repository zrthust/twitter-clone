import React, { useContext } from "react";
import { ArrowIcon, ActionWrapper, LogoIcon, ActionText } from "./elements";
import Button from "../../../components/Button";
import Palette from "../../../contexts/Palette";

const Header = () => {
  const palette = useContext(Palette);
  return (
    <>
      {/*<div style={{ minWidth: "39px" }}/>*/}
      <ActionWrapper palette={palette}>
        <ArrowIcon type="arrow" color="primary" />
      </ActionWrapper>
      <LogoIcon type="logo" color="primary" />
      {/*<Button
        buttonType="primary"
        disabled={true}
        onClick={e => console.log(e)}
        style={{
          minHeight: "30px",
          minWidth: "60px",
          height: "0"
        }}
      >
        Next
      </Button>*/}
      <ActionWrapper palette={palette}>
        <ActionText palette={palette}>Skip for now</ActionText>
      </ActionWrapper>
    </>
  );
};

export default Header;
