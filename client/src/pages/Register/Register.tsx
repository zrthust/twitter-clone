import React, { useState } from "react";
import { Container, Content, HeaderSection, MainSection } from "./elements";
import RegisterData from "./RegisterData";
import Header from "./Header";
import CodeValidation from "./CodeValidation";
import PasswordSetting from "./PasswordSetting";
import ProfilePicture from "./ProfilePicture";
import HeaderPicture from "./HeaderPicture";
import BioDescription from "./BioDescription";
enum RegisterStages {
  AccountConfirmation,
  CodeConfirmation,
  PasswordConfirmation
}
const Register = () => {
  const [stage, setStage] = useState(RegisterStages.AccountConfirmation);
  const nextStage = () => {
    setStage(s => s + 1);
  };
  return (
    <Container>
      <Content>
        <HeaderSection>
          <Header />
        </HeaderSection>
        <MainSection>
          <RegisterData />
          {/*<CodeValidation />*/}
          {/*<PasswordSetting />*/}
          {/*<ProfilePicture />*/}
          {/*<HeaderPicture />*/}
          {/*<BioDescription />*/}
        </MainSection>
        {/*<VerificationModal />*/}
      </Content>
    </Container>
  );
};

export default Register;
