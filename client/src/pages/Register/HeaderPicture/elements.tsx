import styled from "styled-components";
import Icon from "../../../components/Icon";

export const Wrapper = styled.div`
  margin: 59px 0;
  max-height: 9em;
  height: 9em;
`;

export const HeaderWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.3);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const CameraIcon = styled(Icon)`
  height: 1.5em;
`;
export const IconWrapper = styled.div`
  min-width: 39px;
  min-height: 39px;
  width: 39px;
  height: 0;
  border-radius: 999px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    background-color: rgba(255, 255, 255, 0.1);
  }
  &:active {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

export const PhotoWrapper = styled.div`
  max-width: 25%;
`;

export const Photo = styled.img`
  width: 100%;
  object-fit: cover;
  margin-top: -50%;
  margin-left: 15px;
  border: 2px solid white;
  border-radius: 999px;
`;
