import React, { useContext } from "react";
import { Paragraph, Title } from "../elements";
import Palette from "../../../contexts/Palette";
import {
  CameraIcon,
  HeaderWrapper,
  IconWrapper,
  Photo,
  PhotoWrapper,
  Wrapper
} from "./elements";
import DefaultProfilePicture from "../../../assets/default_profile_picture.png";

const HeaderPicture = () => {
  const palette = useContext(Palette);
  return (
    <>
      <Title>Pick a header</Title>
      <Paragraph palette={palette}>
        People who visit your profile will see it. Show your style.
      </Paragraph>
      <Wrapper>
        <HeaderWrapper>
          <IconWrapper>
            <CameraIcon color="white" type="camera" />
          </IconWrapper>
        </HeaderWrapper>
        <PhotoWrapper>
          <Photo src={DefaultProfilePicture} alt="profile picture" />
        </PhotoWrapper>
        <Title
          style={{
            margin: "0"
          }}
        >
          betianu
        </Title>
        <Paragraph
          palette={palette}
          style={{
            margin: "0"
          }}
        >
          @betianu1
        </Paragraph>
      </Wrapper>
    </>
  );
};

export default HeaderPicture;
