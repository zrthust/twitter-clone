import React, { useContext } from "react";
import { InputWrapper, Title } from "../elements";
import Input from "../../../components/Input";
import Link from "../../../components/Link";
import Palette from "../../../contexts/Palette";
import { CharacterTracker } from "./elements";

const RegisterData = () => {
  const palette = useContext(Palette);

  return (
    <>
      <Title>Create your account</Title>
      <InputWrapper>
        <Input type="text" label="Name" handleChange={e => console.log} />
        <CharacterTracker palette={palette}>0/50</CharacterTracker>
      </InputWrapper>
      <InputWrapper>
        <Input type="phone" label="Phone" handleChange={e => console.log} />
      </InputWrapper>
      <Link style={{ marginTop: "15px" }}>Use email instead</Link>
    </>
  );
};

export default RegisterData;
