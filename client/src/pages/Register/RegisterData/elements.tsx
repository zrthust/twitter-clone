import styled from "styled-components";
import { PaletteProps } from "../../../contexts/Palette";

export const CharacterTracker = styled.p<PaletteProps>`
  align-self: flex-end;
  font-size: 15px;
  padding-top: 5px;
  color: ${props => props.palette!.normalGray};
  padding-right: 10px;
`;
