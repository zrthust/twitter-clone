import React, { useContext } from "react";
import { InputWrapper, Paragraph, Title } from "../elements";
import Input from "../../../components/Input";
import Link from "../../../components/Link";
import Palette from "../../../contexts/Palette";

const CodeValidation = () => {
  const palette = useContext(Palette);
  return (
    <>
      <Title>We sent you a code</Title>
      <Paragraph palette={palette}>
        Enter it below to verify 0767300623.
      </Paragraph>
      <InputWrapper>
        <Input
          type="text"
          label="Verification code"
          handleChange={e => console.log}
        />
      </InputWrapper>
      <Link
        style={{
          padding: "0 10px"
        }}
      >
        Didn't receive SMS?
      </Link>
    </>
  );
};

export default CodeValidation;
