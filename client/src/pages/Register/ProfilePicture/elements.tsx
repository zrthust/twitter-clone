import styled from "styled-components";
import Icon from "../../../components/Icon";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PhotoWrapper = styled.div`
  width: 177px;
  max-width: 177px;
  height: 177px;
  max-height: 177px;
  margin: 59px 0;
  border-radius: 9999px;
  border: 2px;
  overflow: hidden;
  position: relative;
  background-color: black;
`;

export const Photo = styled.img`
  width: 100%;
  height: auto;
  object-fit: fill;
  opacity: 0.75;
  user-select: none;
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: calc(50% - 20px);
  left: calc(50% - 20px);
  min-width: 39px;
  min-height: 39px;
  width: 39px;
  height: 0;
  border-radius: 999px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    background-color: rgba(255, 255, 255, 0.1);
  }
  &:active {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

export const CameraIcon = styled(Icon)`
  height: 1.5em;
  width: 100%;
  opacity: 0.75;
`;
