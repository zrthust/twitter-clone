import React, { useContext } from "react";
import { Paragraph, Title } from "../elements";
import Palette from "../../../contexts/Palette";
import DefaultProfilePicture from "../../../assets/default_profile_picture.png";
import {
  CameraIcon,
  IconWrapper,
  Photo,
  PhotoWrapper,
  Wrapper
} from "./elements";
const ProfilePicture = () => {
  const palette = useContext(Palette);
  return (
    <>
      <Title>Prick a profile picture</Title>
      <Paragraph palette={palette}>
        Have a favorite selfie? Upload it now
      </Paragraph>
      <Wrapper>
        <PhotoWrapper>
          <Photo src={DefaultProfilePicture} alt="default profile picture" />
          <IconWrapper>
            <CameraIcon color="white" type="camera" />
          </IconWrapper>
        </PhotoWrapper>
      </Wrapper>
    </>
  );
};

export default ProfilePicture;
