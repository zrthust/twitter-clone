import styled from "styled-components";
import { PaletteProps } from "../../contexts/Palette";
import Icon from "../../components/Icon";

export const Container = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  width: 100%;
  max-width: 575px;
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
  display: flex;
  flex-basis: auto;
  flex-direction: column;
  flex-shrink: 0;
  position: relative;
  align-items: stretch;
`;

export const LogoIcon = styled(Icon)`
  height: 39px;
  position: relative;
  max-width: 100%;
`;

export const Title = styled.h1`
  margin-top: 30px;
  margin-bottom: 10px;
  line-height: 1.3125;
  font-size: 23px;
  font-weight: bold;
  text-align: center;
`;

export const InputWrapper = styled.div`
  padding: 12px 15px;
`;

export const LinkWrapper = styled.div`
  display: flex;
  margin-top: 20px;
  font-size: 15px;
  font-weight: 400;
  align-items: baseline;
  justify-content: center;
  justify-items: center;
  line-height: 1.3125;
  text-align: center;
`;

export const Dot = styled.span`
  padding-left: 5px;
  padding-right: 5px;
  text-decoration: none;
  text-align: center;
  flex-shrink: 0;
`;

export const ErrorMessage = styled.p<PaletteProps>`
  font-size: 15px;
  font-weight: 400;
  line-height: 1.3125;
  color: ${props => props.palette.error};
`;
