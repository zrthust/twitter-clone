import React, { useContext } from "react";
import {
  Container,
  Dot,
  ErrorMessage,
  InputWrapper,
  LinkWrapper,
  LogoIcon,
  Title
} from "./elements";
import Palette from "../../contexts/Palette";
import Input from "../../components/Input";
import Button from "../../components/Button";
import Link from "../../components/Link";

const Login = () => {
  const palette = useContext(Palette);
  return (
    <Container>
      <LogoIcon type="logo" color="primary" />
      <Title>Log in to Twitter</Title>
      <ErrorMessage palette={palette}>
        The username and password you entered did not match our records. Please
        double-check and try again.
      </ErrorMessage>
      <InputWrapper>
        <Input
          type="text"
          label="Phone, email ,or username"
          handleChange={console.log}
        />
      </InputWrapper>
      <InputWrapper>
        <Input type="password" label="Password" handleChange={console.log} />
      </InputWrapper>
      <Button
        buttonType="primary"
        onClick={console.log}
        style={{
          minWidth: "80px",
          minHeight: "49px",
          transitionDuration: "0.2s",
          margin: "10px"
        }}
        disabled={true}
      >
        Log in
      </Button>
      <LinkWrapper>
        <Link to="/reset">Forgot password?</Link>
        <Dot>·</Dot>
        <Link to="/register">Sign up for Twitter</Link>
      </LinkWrapper>
    </Container>
  );
};

export default Login;
