import { createContext } from "react";

export const defaultPalette = {
  primaryColor: "rgb(23,191,99)",
  secondaryColor: "rgb(139,223,177)",
  lightGray: "rgb(245, 248, 250)",
  normalGray: "rgb(101, 119, 134)",
  shallowPrimaryColor: "rgba(23,191,99,0.1)",
  harderShallowPrimaryColor: "rgba(23,191,99,0.2)",
  harderPrimaryColor: "rgb(40,150,55)",
  error: "rgb(224, 36, 94)",
  buttonGray: "rgb(230, 236, 240)",
  buttonGrayHover: "rgb(207,212,216)"
};
export interface PaletteProps {
  palette: {
    primaryColor: string;
    secondaryColor: string;
    lightGray: string;
    normalGray: string;
    shallowPrimaryColor: string;
    harderShallowPrimaryColor: string;
    harderPrimaryColor: string;
    error: string;
    buttonGray: string;
    buttonGrayHover: string;
  };
}
const Palette = createContext(defaultPalette);

export default Palette;
