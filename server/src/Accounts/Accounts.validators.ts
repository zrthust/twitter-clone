import { NextFunction, Request, Response } from "express";
import {
  HttpException,
  InvalidPropertyException,
  MissingMandatoryPropertyException
} from "../Errors";
import { HttpStatus } from "../utils/enums";
import {
  validateConfirmationCode,
  validateEmail,
  validateFlowToken,
  validateName,
  validatePassword,
  validatePhone
} from "../utils/validators";
import { RegisterStages } from "./Accounts.controllers";

export const validateAccountCreation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { flow_token } = req.body;
  let httpException = new HttpException(HttpStatus.OK);
  if (!flow_token) {
    validateAccountConfirmation(req, httpException);
  } else {
    validateFlowTokenStages(req, httpException);
  }
  if (httpException.status === HttpStatus.OK) {
    next();
  } else {
    throw httpException;
  }
};

const validateFlowTokenStages = (
  req: Request,
  httpException: HttpException
) => {
  const { flow_token } = req.body;
  const { isValid, errorMessage, decodedToken } = validateFlowToken(flow_token);
  if (!isValid) {
    httpException
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .addNewException(
        new InvalidPropertyException(errorMessage, "flow_token", "$.flow_token")
      );
  } else {
    const { stage, user } = decodedToken;
    req.app.set("user", user);
    if (stage === RegisterStages.CodeConfirmation) {
      validateCodeConfirmation(req, httpException);
    } else if (stage === RegisterStages.PasswordConfirmation) {
      validatePasswordConfirmation(req, httpException);
    }
  }
};

const validateAccountConfirmation = (
  req: Request,
  httpException: HttpException
) => {
  req.app.set("stage", RegisterStages.AccountConfirmation);
  const { name, email, phone } = req.body;
  if (!(phone || email)) {
    httpException
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .addNewException(
        new MissingMandatoryPropertyException(
          "phone or email",
          "$.phone or $.email"
        )
      );
  } else {
    if (phone) {
      const { isValid, errorMessage } = validatePhone(phone);
      if (!isValid) {
        httpException
          .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
          .addNewException(
            new InvalidPropertyException(errorMessage, "phone", "$.phone")
          );
      }
    } else if (email) {
      const { isValid, errorMessage } = validateEmail(email);
      if (!isValid) {
        httpException
          .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
          .addNewException(
            new InvalidPropertyException(errorMessage, "email", "$.email")
          );
      }
    }
  }
  if (!name) {
    httpException
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .addNewException(new MissingMandatoryPropertyException("name", "$.name"));
  } else {
    const { isValid, errorMessage } = validateName(name);
    if (!isValid) {
      httpException
        .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
        .addNewException(
          new InvalidPropertyException(errorMessage, "name", "$.name")
        );
    }
  }
};
const validateCodeConfirmation = (
  req: Request,
  httpException: HttpException
) => {
  req.app.set("stage", RegisterStages.CodeConfirmation);
  const { code } = req.body;
  const { isValid, errorMessage } = validateConfirmationCode(code);
  if (!code) {
    httpException
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .addNewException(new MissingMandatoryPropertyException("code", "$.code"));
  } else if (!isValid) {
    httpException
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .addNewException(
        new InvalidPropertyException(errorMessage, "code", "$.code")
      );
  }
};
const validatePasswordConfirmation = (
  req: Request,
  httpException: HttpException
) => {
  req.app.set("stage", RegisterStages.PasswordConfirmation);
  const { password } = req.body;
  if (!password) {
    httpException
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .addNewException(
        new MissingMandatoryPropertyException("password", "$.password")
      );
  } else {
    const { isValid, errorMessage } = validatePassword(password);
    if (!isValid) {
      httpException
        .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
        .addNewException(
          new InvalidPropertyException(errorMessage, "password", "$.password")
        );
    }
  }
};
