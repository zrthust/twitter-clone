import { NextFunction, Request, RequestHandler, Response } from "express";
import jwt from "jsonwebtoken";
import { HttpStatus } from "../utils/enums";

export enum RegisterStages {
  AccountConfirmation,
  CodeConfirmation,
  PasswordConfirmation
}
export const createAccount: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const stage = req.app.get("stage");
  if (stage === RegisterStages.AccountConfirmation) {
    accountConfirmation(req, res, next);
  } else if (stage === RegisterStages.CodeConfirmation) {
    codeConfirmation(req, res, next);
  } else if (stage === RegisterStages.PasswordConfirmation) {
    passwordConfirmation(req, res, next);
  }
};

const accountConfirmation: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { phone, email } = req.body;

  // I am totally aware Math.random is horseshit from a security perspective.
  // I'm still using it because it's just a demo app.
  // For a production app there should be a generator that's unguessable
  const code = Math.floor(100000 + Math.random() * 900000).toString();

  const token = jwt.sign(
    {
      user: phone ? phone : email,
      stage: RegisterStages.CodeConfirmation
    },
    process.env.JWT_FLOW_SECRET || "VeryStrongDefault",
    { expiresIn: "1h" }
  );
  // Create in database
  console.log("We created a temporary account");

  res.status(HttpStatus.OK).send({ code, flow_token: token });
};

const codeConfirmation: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = req.app.get("user");
  const token = jwt.sign(
    {
      user,
      stage: RegisterStages.PasswordConfirmation
    },
    process.env.JWT_FLOW_SECRET || "VeryStrongDefault",
    { expiresIn: "1h" }
  );
  res.status(HttpStatus.OK).send({ flow_token: token });
};

const passwordConfirmation: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = req.app.get("user");
  const token = jwt.sign(
    {
      user
    },
    process.env.JWT_LOGIN_SECRET || "VeryStrongDefault"
  );
  res.status(HttpStatus.CREATED).send({ token });
};
