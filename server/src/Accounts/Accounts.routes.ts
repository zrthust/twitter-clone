import express from "express";
import { createAccount } from "./Accounts.controllers";
import { validateAccountCreation } from "./Accounts.validators";
const router = express.Router();

router.post("/", validateAccountCreation, createAccount);
router.post("/login");

router.get("/:username");
router.patch("/:username");
router.delete("/:username");

export default router;
