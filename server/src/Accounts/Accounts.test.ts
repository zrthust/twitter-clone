import { createAccount } from "./Accounts.controllers";
import { Request } from "express";
import { validateAccountCreation } from "./Accounts.validators";
import { HttpStatus } from "../utils/enums";
import {
  InvalidPropertyException,
  MissingMandatoryPropertyException
} from "../Errors";

let req: Request;
let res: any;
let next: any;
let resSet: any;
let resStatus: any;
let resJson: any;
let resSend: any;

function setUpExpressMocks() {
  resJson = jest.fn();
  resStatus = jest.fn();
  resSet = jest.fn();
  resSend = jest.fn();
  res = {
    set: resSet,
    status: resStatus,
    json: resJson,
    send: resSend
  };
  resJson.mockImplementation(() => res);
  resStatus.mockImplementation(() => res);
  resSet.mockImplementation(() => res);
  resSend.mockImplementation(() => res);
  req = {} as Request;
  next = jest.fn();
}

// TODO: fully test the controller and validator

describe("[ACCOUNTS] Controllers tests", () => {
  beforeEach(setUpExpressMocks);
  it("STAGE-0 | should return a 6 digit code and a flow token", () => {
    // Arrange
    req.body = {
      name: "Alexandru Razvan",
      phone: "0767300623"
    };
    // Act
    createAccount(req, res, next);
    // Assert
    expect(res.status).toBeCalledWith(201);
    expect(res.send).toHaveBeenCalledTimes(1);
    expect(res.send.mock.calls[0][0].code).toMatch(/^[0-9]{6}$/);
    expect(res.send.mock.calls[0][0].flow_token).not.toBeFalsy();
  });
});

describe("[ACCOUNTS] Validators tests", () => {
  beforeEach(setUpExpressMocks);
  it("STAGE-0 | should validate an account if everything provided is valid", () => {
    // Arrange
    req.body = {
      phone: "0767300623",
      name: "Alexandru"
    };
    // Act
    validateAccountCreation(req, res, next);
    // Assert
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
  });
  it("STAGE-0 | should not validate an account if a mandatory property is missing", () => {
    // Arrange
    req.body = {
      phone: "0767300623"
    };
    try {
      // Act
      validateAccountCreation(req, res, next);
      // Fail test if this is reached
      expect(true).toBeFalsy();
    } catch (e) {
      // Assert
      expect(e.status).toBe(HttpStatus.UNPROCESSABLE_ENTITY);
      expect(e.errors.length).toBe(1);
      expect(
        e.errors[0] instanceof MissingMandatoryPropertyException
      ).toBeTruthy();
    }
  });
  it("STAGE-0 | should not validate an account if a property is invalid", () => {
    // Arrange
    req.body = {
      phone: "0767300623",
      name: "Al"
    };
    try {
      // Act
      validateAccountCreation(req, res, next);
      // Fail test if this is reached
      expect(true).toBeFalsy();
    } catch (e) {
      // Assert
      expect(e.status).toBe(HttpStatus.UNPROCESSABLE_ENTITY);
      expect(e.errors.length).toBe(1);
      expect(e.errors[0] instanceof InvalidPropertyException).toBeTruthy();
    }
  });
});
