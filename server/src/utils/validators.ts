import validator from "validator";
import jwt from "jsonwebtoken";
interface BasicValidatorInterface {
  isValid: boolean;
  errorMessage: string;
}
export const validateName = (name: string): BasicValidatorInterface => {
  if (name.length >= 3) {
    if (name.length > 50) {
      return {
        isValid: false,
        errorMessage: `The name needs to have a maximum of 50 characters. Your provided value was: ${name}. Example of a valid name: alexandru`
      };
    }
    return { isValid: true, errorMessage: "" };
  } else {
    return {
      isValid: false,
      errorMessage: `The name needs to be at least 3 characters long. Your provided value was: ${name}. Example of a valid name: alexandru`
    };
  }
};

export const validatePassword = (password: string): BasicValidatorInterface => {
  if (password.length >= 6) {
    return {
      isValid: true,
      errorMessage: ""
    };
  } else {
    return {
      isValid: false,
      errorMessage: `The password needs to be at least 6 characters long. Example of a valid password: alexandru`
    };
  }
};

export const validateEmail = (email: string): BasicValidatorInterface => {
  if (validator.isEmail(email)) {
    return { isValid: true, errorMessage: "" };
  } else {
    return {
      isValid: false,
      errorMessage: `The provided email is not valid. Your provided value was: ${email}. Example of a valid email: alex@gmail.com`
    };
  }
};
export const validatePhone = (phone: string): BasicValidatorInterface => {
  const regex = RegExp("^[0-9]{10}$");
  if (regex.test(phone)) {
    return { isValid: true, errorMessage: "" };
  } else {
    return {
      isValid: false,
      errorMessage: `Provide a phone number that has exactly 10 digits. Your provided value was: ${phone}. Example of a valid phone number: 0789425786`
    };
  }
};

export const validateConfirmationCode = (
  code: string
): BasicValidatorInterface => {
  const regex = RegExp("^[0-9]{6}$");
  if (regex.test(code)) {
    return {
      isValid: true,
      errorMessage: ""
    };
  } else {
    return {
      isValid: false,
      errorMessage: `A confirmation code has exactly 6 digits. Your code was ${code}. Example of a valid code: 123456`
    };
  }
};

export const validateFlowToken = (flowToken: string) => {
  try {
    const decodedToken = jwt.verify(
      flowToken,
      process.env.JWT_FLOW_SECRET || "VeryStrongDefault"
    );
    return {
      isValid: true,
      errorMessage: "",
      decodedToken: decodedToken as any
    };
  } catch (e) {
    return {
      isValid: false,
      errorMessage:
        "The provided flow token was not valid. It might be changed or expired. Retry registration from the beginning if this continues to happen."
    };
  }
};
