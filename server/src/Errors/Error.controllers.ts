import { NextFunction, Request, Response } from "express";
import { HttpException, ValidationException } from "./Error.types";

export const errorHandler = (
  err: HttpException | any,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof HttpException) {
    let errors = err.errors.map((e: ValidationException) => e.toJson());
    return res.status(err.status).send({ message: err.message, errors });
  }
  console.log(err);
  return res
    .status(err.status || 500)
    .send({ message: "There was a server error" });
};
