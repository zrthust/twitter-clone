import { HttpStatus } from "../utils/enums";
import ExtendableError from "es6-error";

export enum ExceptionTypes {
  MISSING_MANDATORY_PROPERTY,
  INVALID_PROPERTY,
  INVALID_TOKEN,
  FUNCTIONAL_ERROR,
  UNEXPECTED_SERVER_ERROR
}

export interface ValidationExceptionInterface {
  source: string;
  type: ExceptionTypes;
  message: string;
  path: string;
}

interface HttpErrorInterface {
  status: number;
  message: string;
  errors: ValidationExceptionInterface[];
}

export class ValidationException implements ValidationExceptionInterface {
  message: string;
  path: string;
  source: string;
  type: ExceptionTypes;
  constructor(
    message: string,
    path: string,
    source: string,
    type: ExceptionTypes
  ) {
    this.message = message;
    this.path = path;
    this.source = source;
    this.type = type;
  }
  toJson() {
    return {
      message: this.message,
      type: ExceptionTypes[this.type],
      source: this.source,
      path: this.path
    };
  }
}

export class MissingMandatoryPropertyException extends ValidationException {
  constructor(property: string, path: string) {
    super(
      `You need to provide ${property.toUpperCase()} because it's mandatory`,
      path,
      property,
      ExceptionTypes.MISSING_MANDATORY_PROPERTY
    );
  }
}

export class InvalidPropertyException extends ValidationException {
  constructor(message: string, source: string, path: string) {
    super(message, path, source, ExceptionTypes.INVALID_PROPERTY);
  }
}

export class HttpException extends ExtendableError {
  status: HttpStatus;
  errors: ValidationException[];
  constructor(status: number) {
    super("There was an error in processing your request.");
    this.status = status;
    this.errors = [];
  }
  setStatus(status: HttpStatus) {
    this.status = status;
    return this;
  }
  addNewException(exception: ValidationException) {
    this.errors.push(exception);
    return this;
  }
}
