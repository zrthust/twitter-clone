import express from "express";
import helmet from "helmet";
import cors from "cors";
import bodyParser from "body-parser";
import env from "dotenv";
import { AccountsRoutes } from "./src/Accounts";
import { ErrorRoutes } from "./src/Errors";

env.config();
const app = express();

app.use(helmet());
app.use(cors());
app.use(bodyParser.json());

app.get("/", (_: any, res: any) => {
  res.send("alex");
});

app.use("/accounts", AccountsRoutes);
app.use(ErrorRoutes);
export default app;
